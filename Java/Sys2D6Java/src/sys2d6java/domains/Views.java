/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys2d6java.domains;

/**
 * Classe de visualização dos Personagens
 * seus atributos e as alterações consecutivas
 * @author Luke Frozz
 */
public class Views {
    
    public static void PersonagembyRaca(Personagem p) {
        StringBuilder sb = new StringBuilder();
        
        sb.append("|------------------------------|\n");
        sb.append(String.format("|Personagem: %18s|\n", p.getNome()));
        sb.append("|------------------------------|\n");
        sb.append(String.format("|Idade: %23s|\n", (
            p.getIdade() == null ? "desconhecida" : p.getIdade())));
        sb.append(String.format("|Sexo: %24s|\n", p.getSexo()));
        sb.append(String.format("|Tendência: %19s|\n", 
            String.format("%s e %s", p.getTendencia().getEtica(),
                p.getTendencia().getMoral())));
        sb.append(String.format("|%30s|\n", 
            String.format("%s / %s",
                p.getTendencia().getNomePT(),
                p.getTendencia().getNomeING())));
        sb.append(String.format("|Raça: %24s|\n", p.getRaca().getNome()));
        sb.append("|-----------ATRIBUTOS----------|\n");
        sb.append("|FOR: %25s|");
        sb.append("|DES: %25s|");
        sb.append("|CONS: %24s|");
        sb.append("|INT: %25s|");
        sb.append("|SAB: %25s|");
        sb.append("|CAR: %25s|");
        System.out.println(sb);
    }
}
