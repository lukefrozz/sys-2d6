/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys2d6java.domains;

import java.time.LocalDate;
import java.util.*;

/**
 * Classe representativa da Campanha
 * @author Luke Frozz
 * @since 23/09/2015
 */
public class Campanha {
    private Long id;
    private String nome;
    private String descricao;
    private Jogador mestre;
    private LocalDate dataCriacao;
    private List<Personagem> personagens;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Jogador getMestre() {
        return mestre;
    }

    public void setMestre(Jogador mestre) {
        this.mestre = mestre;
    }

    public LocalDate getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(LocalDate dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public List<Personagem> getPersonagens() {
        return personagens;
    }

    public void setPersonagens(List<Personagem> personagens) {
        this.personagens = personagens;
    }
}
