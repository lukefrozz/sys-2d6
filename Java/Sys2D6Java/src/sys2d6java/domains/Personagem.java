/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys2d6java.domains;

import java.util.List;

/**
 * Classe Representativa de um Personagem
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class Personagem {
    private Long id;
    private String nome;
    private Integer idade;
    private Sexo sexo;
    private Raca raca;
    private Atributos atributos;
    private List<SlotPericia> pericias;
    private Tendencia tendencia;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Raca getRaca() {
        return raca;
    }

    public void setRaca(Raca raca) {
        this.raca = raca;
    }

    public Atributos getAtributos() {
        return atributos;
    }

    public void setAtributos(Atributos atributos) {
        this.atributos = atributos;
    }

    public List<SlotPericia> getPericias() {
        return pericias;
    }

    public void setPericias(List<SlotPericia> pericias) {
        this.pericias = pericias;
    }

    public Tendencia getTendencia() {
        return tendencia;
    }

    public void setTendencia(Tendencia tendencia) {
        this.tendencia = tendencia;
    }

    public Personagem() {
    }
    
}
