/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys2d6java.domains;

/**
 * Classe representativa de um Slot de perícia
 * Onde contém a perícia tida pelo personagem junto
 * a sua especialização da mesma
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class SlotPericia {
    private Pericia pericia;
    private Integer pontos;

    public Pericia getPericia() {
        return pericia;
    }

    public void setPericia(Pericia pericia) {
        this.pericia = pericia;
    }

    public Integer getPontos() {
        return pontos;
    }

    public void setPontos(Integer pontos) {
        this.pontos = pontos;
    }
    
    
}
