/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys2d6java.domains;

import java.util.*;

/**
 * Classe representativa do Jogador
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class Jogador {
    private Long id;
    private String nome;
    private List<Personagem> personagens;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Personagem> getPersonagens() {
        return personagens;
    }

    public void setPersonagens(List<Personagem> personagens) {
        this.personagens = personagens;
    }

    public Jogador() {
    }
}
