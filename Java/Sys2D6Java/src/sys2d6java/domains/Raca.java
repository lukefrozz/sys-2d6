/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys2d6java.domains;

import java.util.List;

/**
 * classe representativa de Raça
 * @author Luke Frozz
 * @since 21/09/2015
 */
public class Raca {
    private Long id;
    private String Nome;
    private String Descricao;
    private Atributos alteracaoAtributos;
    private List<SlotPericia> alteracaoPericias;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getDescricao() {
        return Descricao;
    }

    public void setDescricao(String Descricao) {
        this.Descricao = Descricao;
    }

    public Atributos getAlteracaoAtributos() {
        return alteracaoAtributos;
    }

    public void setAlteracaoAtributos(Atributos alteracaoAtributos) {
        this.alteracaoAtributos = alteracaoAtributos;
    }

    public List<SlotPericia> getAlteracaoPericias() {
        return alteracaoPericias;
    }

    public void setAlteracaoPericias(List<SlotPericia> alteracaoPericias) {
        this.alteracaoPericias = alteracaoPericias;
    }
    
}
