/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys2d6java.domains;

/**
 * Enumerador de Sexo
 * @author Luke Frozz
 * @since 23/09/2015
 */
public enum Sexo {
    Masculino,
    Feminino
}
