/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys2d6java.domains;

/**
 * Classe representativa de uma Tendência
 * @author Luke Frozz
 * @since 23/09/2015
 */
public class Tendencia {
    private Long id;
    private String nomePT;
    private String nomeING;
    private String descricao;
    private Integer moral;
    private Integer etica;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomePT() {
        return nomePT;
    }

    public void setNomePT(String nomePT) {
        this.nomePT = nomePT;
    }

    public String getNomeING() {
        return nomeING;
    }

    public void setNomeING(String nomeING) {
        this.nomeING = nomeING;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getMoral() {
        String typeM = "";
        switch (moral) {
            case 1:
                typeM = "Bom";
                break;
            case 2:
                typeM = "Neutro";
                break;
            case 3:
                typeM = "Mal";
        }
        return typeM;
    }

    public void setMoral(Integer moral) {
        this.moral = moral;
    }

    public String getEtica() {
        String typeE = "";
        switch (etica) {
            case 1:
                typeE = "Leal";
                break;
            case 2:
                typeE = "Neutro";
                break;
            case 3:
                typeE = "Caótico";
        }
        return typeE;
    }

    public void setEtica(Integer etica) {
        this.etica = etica;
    }
    
    
}
