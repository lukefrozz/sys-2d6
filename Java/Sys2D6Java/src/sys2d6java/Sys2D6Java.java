/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sys2d6java;

import sys2d6java.domains.*;
import sys2d6java.domains.Views;

/**
 *
 * @author lucas.farias
 */
public class Sys2D6Java {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Personagem p = new Personagem();
        p.setNome("Lucius");
        p.setIdade(19);
        p.setSexo(Sexo.Masculino);
        Tendencia t = new Tendencia();
        t.setNomePT("Benfeitor");
        t.setNomeING("Neutral Good");
        t.setMoral(1);
        t.setEtica(2);
        p.setTendencia(t);
        Raca r = new Raca();
        r.setNome("Humano");
        p.setRaca(r);
        Views.PersonagembyRaca(p);
    }
    
}
